#!/usr/bin/python

import socket
import random

urls = ['http://gsyc.es/', 'http://www.google.com/', 'http://www.aulavirtual.urjc.es/', 'http://www.wikipedia.com/']

PORT = 1234

if __name__ == '__main__':
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # AF_INET - IPv4
    # SOCK_STREAM - TCP

    # Voy a arreglar lo del puerto ocupado
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Anclo el socket
    mySocket.bind(('localhost', PORT))

    # Meto en cola un maximo de 5 conexiones TCP
    mySocket.listen(5)

    while True:
        print("Esperando alguna conexion...")
        connectionSocket, addr = mySocket.accept()
        print("Conexion recibida de: " + str(addr))
        recibido = connectionSocket.recv(2048)
        print(recibido)

        # Redirigir a un recurso aleatorio
        recurso = "HTTP/1.1 308 Permanent Redirect\r\n\r\n" \
                    + "<html><body><h1>Redirecting..."  \
                    + "<a href='" + str(random.randint(0, 10000)) \
                    + "'>Recurso aleatorio</a>" \
                    + "</h1></body></html>" \
                    + "\r\n"

        # Redirigir a una URL aleatoria
        redireccion = "HTTP/1.1 308 Permanent Redirect\r\n" \
                        + "<html><body><h1>Redirecting... </h1></body></html>\r\n" \
                        + "location:" + urls[random.randint(0, len(urls) - 1)] \
                        + "\r\n"

        redireccion_propia = "HTTP/1.1 308 Permanent Redirect\r\n" \
                                + "<html><body><h1>Redirecting... </h1></body></html>\r\n" \
                                + "Location: http://localhost:" + str(PORT) \
                                + "\r\n"

        # Codificar a UTF-8 para poder enviar el mensaje. Se debe elegir entre utilizar recurso o redireccion
        connectionSocket.send(recurso.encode('utf-8'))
        connectionSocket.close()
